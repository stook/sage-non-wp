# [Sage non-wp theme development by Stook](https://stook.io/)

Explaining the full process of developing in a trellis/bedrock/sage environment can be quite much to take in for frontend developers that havent used it before. At the same time a pure html/css delivery often takes long time to pick apart and implement into the real sage theme. 

This is middle way to get a developer up and running fast but still keep the same structure and build pipeline as the sage theme as much as possible without even having WP installed. 

**Stripped:**

* Theme wrapper functionality and inheritance. Use simple php includes instead.
* Setup and the sage libs. Simplify the frontend development with minimal knowledge of PHP (php include is actually enougth)  

**Still in**

* A modern build process (nvm, gulp, bower, browsersync)
* Asset-builder (manifest.json)
* The sage asset structure and by using simple "php includes" keep it as dry as possible 


**Source**

* [Sage non-wp theme development by Stook](https://bitbucket.org/stook/sage-non-wp)  
* Original Sage theme source: [https://github.com/roots/sage](https://github.com/roots/sage)

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| Node.js >= 4.5  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| Bower >= 1.3.12 | `bower -v`   | `npm install -g bower` |

For more installation notes, refer to the [Install gulp and Bower](#install-gulp-and-bower) section in this document.

## Features

* [gulp](http://gulpjs.com/) build script that compiles both Sass and Less, checks for JavaScript errors, optimizes images, and concatenates and minifies files
* [BrowserSync](http://www.browsersync.io/) for keeping multiple browsers and devices synchronized while testing, along with injecting updated CSS and JS into your browser while you're developing
* [Bower](http://bower.io/) for front-end package management
* [asset-builder](https://github.com/austinpray/asset-builder) for the JSON file based asset pipeline
* [Bootstrap](http://getbootstrap.com/)


### Install gulp and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process. Remember to use bower for new dependencis as they will be autowired during the build process.

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

* quirks: if image or font folder is not already created `gulp watch` will not create those folders. Exit and run a `gulp` to get the flow running when you add the first image or font. 

### Using BrowserSync

To use BrowserSync during `gulp watch` you need to update `devUrl` at the bottom of `assets/manifest.json` to reflect your local development hostname.

For example, if your local development URL is `http://project-name.dev` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://project-name.dev"
  }
...
```
If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```


## Local server setup

**If you have nothing installed**

Keep your local environment clean from server/databases/frameworks and use a VM. Try one of the lamp/lemp vagrant boxes out there to keep your local environment clean. We used [scotchbox](https://github.com/scotch-io/scotch-box) successfully to test this code out.   

**If you have Apache and PHP up and running locally already**

Win version: 

* Create an entry in your virtual-host file in your apache conf folder. Ex:

```
<VirtualHost *:80>
        ServerName <project-name.dev>
        DocumentRoot "d:\projects\<project-name>"
</VirtualHost>
```

* Create an entry in your host file `C:\Windows\System32\drivers\etc\hosts`

```
127.0.0.1 project-name.dev
```

* See the "Using BrowserSync" section to configure your local hostname in `assets/manifest.json`
* Restart your apache server and get coding


## Thanks

**Big thanks from [STOOK](http://stook.io) goes to the [ROOTS](http://roots.io) team for getting some structure in the wordpress development community!**