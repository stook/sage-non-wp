<!doctype html>
<html>
<?php include 'templates/head.php';?>
<body>
<!--[if IE]>
<div class="alert alert-warning">
  You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->
<?php include 'templates/header.php';?>
<div class="wrap container" role="document">
  <div class="content row">
    <main class="main">
        The content
    </main>
    <aside class="sidebar">
      <?php include 'templates/sidebar.php';?>
    </aside>
  </div>
</div>
<?php include 'templates/footer.php';?>
<script src="dist/scripts/jquery.js"></script>
<script src="dist/scripts/main.js"></script>
</body>
</html>
